#include "DHT.h"
#include  <WiFi.h>
#define DHTPIN 12
#define DHTTYPE DHT11
float pino_sinal_analogico = 2;

float localHum = 0;
float localTemp = 0;
float valor_analogico = 0;
DHT dht(DHTPIN, DHTTYPE);

const char *ssid = "Luiz_WIFI";
const char *password = "silva0927";
WiFiServer server(80);

const char *host = "api.thingspeak.com";
String api_key = "HSWAGT9GQP6TM469";

void setup()
{
  Serial.begin(115200);
  pinMode(pino_sinal_analogico, INPUT);

  Serial.print("Connecting to: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();

  dht.begin();
}

void loop()
{
  WiFiClient client = server.available();
  dhtLoop();
  sendData();
}

void dhtLoop()
{
  getDHT();
  Serial.print("Local Temp: ");
  Serial.print(localTemp);
  Serial.print("Local Hum: ");
  Serial.println(localHum);
  Serial.print("valor_analogico: ");
  Serial.println(valor_analogico);
  delay(2000);
}

void getDHT()
{
  localTemp = dht.readTemperature();
  localHum = dht.readHumidity();

  valor_analogico = analogRead(pino_sinal_analogico);
}

void sendData()
{
  Serial.println("Prepare to send data");

  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort))
  {
    Serial.println("connection failed");
    return;
  }
  else
  {
    String data_to_send = api_key;
    data_to_send += "&field1=";
    data_to_send += String(localTemp);
    data_to_send += "&field2=";
    data_to_send += String(valor_analogico);
    data_to_send += "&field3=";
    data_to_send += String(localHum);
    data_to_send += "\r\n\r\n";

    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + api_key + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(data_to_send.length());
    client.print("\n\n");
    client.print(data_to_send);

    delay(300000);
  }

  client.stop();
}