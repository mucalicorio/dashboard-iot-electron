var rp = require('request-promise');

var registros = [];
var dataTemperatura = [];
var dataUmidade = [];
var dataUmidadeSolo = [];

var time = [];
API_URL = 'https://api.thingspeak.com/channels/777490/feeds.json?results'
var options = {
    method: 'GET',
    uri: API_URL,
    body: {
    },
    json: true
};

setInterval(function () {
    getAPI();
}, 1000);

function mudandacaValores(campo_id, valor) {
    document.querySelector(campo_id).textContent = '';
    if (campo_id == '#temperatura') {
        document.querySelector(campo_id).textContent = valor + 'º';
    } else if (campo_id == '#umd_ar') {
        document.querySelector(campo_id).textContent = valor + '%';
    } else if (campo_id == '#umd_solo') {
        document.querySelector(campo_id).textContent = valor + '%';
    } else {
        document.querySelector(campo_id).textContent = valor;
    }
};

function getAPI() {
    rp(options).then(function (parsedBody) {
        registros = parsedBody.feeds

        for (let index = 0; index < registros.length; index++) {
            if (registros.length > dataTemperatura.length) {
                dataTemperatura.push(registros[index].field1)
                dataUmidade.push(registros[index].field3)
                // dataUmidadeSolo.push(registros[index].field2)
                time.push(registros[index].created_at.substring(11, 19))

                var bodyString = '';
                $.each(time, function (index, horario) {
                    bodyString += ('<tr><td>' + horario + '</td> <td>' + dataTemperatura[index] + '</td> <td>' + dataUmidade[index] + '</td></tr>');
                });

                $('.table tbody').html(bodyString);

                gerarGrafico(dataTemperatura, dataUmidade, time)
            }
        }

        const ultimo_registro = registros[registros.length - 1]

        mudandacaValores('#temperatura', ultimo_registro.field1);
        mudandacaValores('#umd_ar', ultimo_registro.field3);
        // mudandacaValores('#umd_solo', ultimo_registro.field2);
        const umidade = Number(ultimo_registro.field3);
        if (umidade < 40) {
            document.getElementById("alerta").style.display = "-webkit-box";
        } else {
            document.getElementById("alerta").style.display = "none";
        }
    }).catch(function (err) {
        console.log(err);
        alert("Deu erro! ");
    })
}


$(document).ready(function () {
    const { remote } = require('electron');
    document.getElementById("alerta").style.display = "none";
    getAPI();

    // JANELA
    $('#minimizar').click(function () {
        remote.BrowserWindow.getFocusedWindow().minimize();
    });
    $('#maximizar').click(function () {
        if (remote.BrowserWindow.getFocusedWindow().isMaximized()) {
            remote.BrowserWindow.getFocusedWindow().restore();
        } else {
            remote.BrowserWindow.getFocusedWindow().maximize();
        }
    });
    $('#fechar').click(function () {
        remote.BrowserWindow.getFocusedWindow().close();
    });

    // ESTILOS
    colors = [
        '#1ab394',
        '#23c6c8',
        '#f8ac59',
        '#23c6c8',
        '#ed5565',
        '#9B9BC4',
        '#E5F77D',
        '#DEBA6F',
        '#F9C9BD',
        '#823038',
    ];

    document.querySelectorAll('.card').forEach(element => {
        element.style.backgroundColor = colors[Math.floor(Math.random() * (colors.length - 1))]
    });

});